<?php

namespace Deha\GeoIp;

use Bitrix\Main\ArgumentException;
use Bitrix\Main\DI\ServiceLocator;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ObjectNotFoundException;
use Bitrix\Main\ObjectPropertyException;
use Bitrix\Main\SystemException;
use Bitrix\Main\Web\Http\Request;
use Deha\GeoIp\Exception\GeoIpException;
use Deha\GeoIp\Services\HlBlockGeoIp;
use Exception;
use Psr\Container\NotFoundExceptionInterface;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Log;

class GeoIpClient implements Log\LoggerAwareInterface
{
    use Log\LoggerAwareTrait;

    protected ClientInterface $client;
    private Services\GeoIpService $service;
    private HlBlockGeoIp $hlBlockGeoIp;

    /**
     * @throws ObjectNotFoundException
     * @throws NotFoundExceptionInterface
     */
    public function __construct()
    {
        $this->client = ServiceLocator::getInstance()->get('deha.geoip.client');
        $this->service = ServiceLocator::getInstance()->get('deha.geoip.service');
        $this->hlBlockGeoIp = new HlBlockGeoIp();
        $this->setLogger(new Log\NullLogger());
    }

    /**
     * @param string $ip
     * @return IpInfo
     * @throws ClientExceptionInterface
     * @throws GeoIpException
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws SystemException
     * @throws Exception
     */
    public function getInfo(string $ip): IpInfo
    {
        $this->logger->debug('Request information for ip :' . $ip);
        $ip = trim($ip);
        $info = $this->hlBlockGeoIp->getInfo($ip);
        if ($info === null) {
            $info = $this->getFromService($ip);
            if ($info !== null) {
                $this->logger->debug('Data received from ' . get_class($this->service) . ' : ' . $info);
                $this->hlBlockGeoIp->save($ip, $info);
                $this->logger->debug('Info save to HL');
            }
        } else {
            $this->logger->debug('Data received from HL block: ' . $info);
        }
        if ($info === null) {
            $this->logger->error('Failed to get information');
            throw new GeoIpException(Loc::getMessage('DGF_FAILED_TO_GET_INFORMATION'));
        }
        return $info;
    }

    /**
     * @param string $ip
     * @return IpInfo|null
     * @throws ClientExceptionInterface
     */
    private function getFromService(string $ip): ?IpInfo
    {
        $request = new Request(
            $this->service->getMethod(),
            $this->service->getUrl($ip)
        );
        return $this->service->parseResult(
            $this->client->sendRequest($request)
        );
    }
}

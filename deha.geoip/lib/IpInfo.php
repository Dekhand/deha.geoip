<?php

namespace Deha\GeoIp;

use Bitrix\Main\ArgumentException;
use Bitrix\Main\Web\Json;

class IpInfo
{
    public string $city;

    public function __construct(
        string $city,
    )
    {
        $this->city = $city;
    }

    /**
     * @return string
     * @throws ArgumentException
     */
    public function __toString(): string
    {
        return Json::encode($this->toArray(), JSON_UNESCAPED_UNICODE);
    }

    public function toArray(): array
    {
        return [
            'city' => $this->city,
        ];
    }


}

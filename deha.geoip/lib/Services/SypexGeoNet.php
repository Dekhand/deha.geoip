<?php

namespace Deha\GeoIp\Services;

use Bitrix\Main\ArgumentException;
use Bitrix\Main\Web\HttpClient;
use Bitrix\Main\Web\Json;
use Bitrix\Main\Web\Uri;
use Deha\GeoIp\IpInfo;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriInterface;

class SypexGeoNet implements GeoIpService
{
    public function getMethod(): string
    {
        return HttpClient::HTTP_GET;
    }

    public function getUrl(string $ip): UriInterface
    {
        return new Uri('https://api.sypexgeo.net/json/' . $ip);
    }

    /**
     * @param ResponseInterface $response
     * @return IpInfo
     * @throws ArgumentException
     */
    public function parseResult(ResponseInterface $response): IpInfo
    {
        $data = Json::decode((string)$response->getBody());
        return new IpInfo(
            $data['city']['name_ru']
        );
    }
}

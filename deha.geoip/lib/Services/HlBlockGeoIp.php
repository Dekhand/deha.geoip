<?php

namespace Deha\GeoIp\Services;

use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\ArgumentException;
use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use Bitrix\Main\ObjectPropertyException;
use Bitrix\Main\ORM\Data\DataManager;
use Bitrix\Main\SystemException;
use Deha\GeoIp\Exception\GeoIpException;
use Deha\GeoIp\IpInfo;
use Exception;

class HlBlockGeoIp
{
    private DataManager $hl;

    /**
     * @throws LoaderException
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws SystemException
     */
    public function __construct()
    {
        Loader::requireModule('highloadblock');
        $arHLBlock = HighloadBlockTable::getList([
            'filter' => [
                '=TABLE_NAME' => 'deha_geoip_hl',
            ],
        ])->fetch();
        $obEntity = HighloadBlockTable::compileEntity($arHLBlock);
        $this->hl = new ($obEntity->getDataClass());
    }

    /**
     * @param string $ip
     * @return IpInfo|null
     * @throws ArgumentException
     * @throws ObjectPropertyException
     * @throws SystemException
     */
    public function getInfo(string $ip): ?IpInfo
    {
        $result = $this->hl::query()
            ->where('UF_IP', $ip)
            ->addSelect('UF_CITY')
            ->fetch();
        if ($result !== false) {
            return new IpInfo(
                $result['UF_CITY']
            );
        }
        return null;
    }

    /**
     * @param string $ip
     * @param IpInfo $info
     * @return void
     * @throws GeoIpException
     * @throws Exception
     */
    public function save(string $ip, IpInfo $info): void
    {
        $obResult = $this->hl::add([
            'UF_IP' => $ip,
            'UF_CITY' => $info->city,
        ]);
        if (!$obResult->isSuccess()) {
            throw new GeoIpException(implode(',', $obResult->getErrorMessages()));
        }
    }
}

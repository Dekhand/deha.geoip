<?php

namespace Deha\GeoIp\Services;

use Bitrix\Main\ArgumentException;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Web\HttpClient;
use Bitrix\Main\Web\Json;
use Bitrix\Main\Web\Uri;
use Deha\GeoIp\Exception\GeoIpException;
use Deha\GeoIp\IpInfo;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriInterface;

class IpStackCom implements GeoIpService
{
    public function getMethod(): string
    {
        return HttpClient::HTTP_GET;
    }

    public function getUrl(string $ip): UriInterface
    {
        /** @noinspection HttpUrlsUsage */
        return (new Uri('http://api.ipstack.com/' . $ip))
            ->addParams([
                'access_key' => Option::get('deha.geoip', 'IpStackComKey') . '1',
            ]);
    }

    /**
     * @param ResponseInterface $response
     * @return IpInfo
     * @throws ArgumentException
     * @throws GeoIpException
     */
    public function parseResult(ResponseInterface $response): IpInfo
    {
        $data = Json::decode((string)$response->getBody());
        if (isset($data['error'])) {
            throw new GeoIpException($data['error']['info'], $data['error']['code']);
        }
        return new IpInfo(
            $data['region_name']
        );
    }
}

<?php

namespace Deha\GeoIp\Services;

use Deha\GeoIp\IpInfo;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriInterface;

interface GeoIpService
{
    public function getMethod(): string;

    public function getUrl(string $ip): UriInterface;

    public function parseResult(ResponseInterface $response): IpInfo;
}

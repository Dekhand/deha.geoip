<?php

use Bitrix\Highloadblock as HL;
use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\Application;
use Bitrix\Main\ArgumentException;
use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\ObjectPropertyException;
use Bitrix\Main\ORM\Query\Query;
use Bitrix\Main\SystemException;

Loc::loadMessages(__FILE__);

class deha_geoip extends CModule
{
    var $MODULE_ID = 'deha.geoip';
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $PARTNER_NAME;
    var $PARTNER_URI;
    var $MODULE_GROUP_RIGHTS = 'Y';

    public function __construct()
    {
        $arModuleVersion = [];
        include __DIR__ . '/version.php';
        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }

        $this->MODULE_NAME = Loc::getMessage('DEHA_GEOIP_MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('DEHA_GEOIP_MODULE_DESCRIPTION');
        $this->PARTNER_NAME = Loc::getMessage('DEHA_GEOIP_PARTNER_NAME');
        $this->PARTNER_URI = Loc::getMessage('DEHA_GEOIP_PARTNER_URI');
    }

    public function DoInstall(): void
    {
        ModuleManager::registerModule($this->MODULE_ID);

        $this->InstallFiles();
        $this->InstallDB();
        $this->InstallEvents();
        $this->InstallTasks();

        LocalRedirect('/geoip/');
    }

    public function InstallFiles(): void
    {
        $basePath = dirname(Loader::getLocal('modules/' . 'deha.geoip' . '/install/index.php'), 4);
        CopyDirFiles(
            __DIR__ . '/files/components',
            $basePath . '/components',
            true,
            true
        );
        $documentRoot = Application::getDocumentRoot();
        CopyDirFiles(
            __DIR__ . '/files/public',
            $documentRoot,
            true,
            true
        );
    }

    /**
     * @return void
     * @throws ArgumentException
     * @throws LoaderException
     * @throws ObjectPropertyException
     * @throws SystemException
     * @throws Exception
     */
    public function InstallDB(): void
    {
        Loader::IncludeModule('highloadblock');
        /** @var Query $hlQuery */
        $hlQuery = HL\HighloadBlockTable::query();
        $arTable = array_column($hlQuery
            ->where('TABLE_NAME', 'deha_geoip_hl')
            ->addSelect('ID')
            ->addSelect('TABLE_NAME')
            ->fetchAll(),
            'ID',
            'TABLE_NAME');

        if (!isset($arTable['deha_geoip_hl'])) {
            $hlAddResult = HL\HighloadBlockTable::add([
                'NAME' => 'GetIp',
                'TABLE_NAME' => 'deha_geoip_hl',
            ]);
            if ($hlAddResult->isSuccess()) {
                HL\HighloadBlockLangTable::add([
                    'ID' => $hlAddResult->getId(),
                    'LID' => 'ru',
                    'NAME' => 'GetIp',
                ]);
                $ufObject = 'HLBLOCK_' . $hlAddResult->getId();
                $arFields = [
                    'UF_IP' => [
                        'ENTITY_ID' => $ufObject,
                        'FIELD_NAME' => 'UF_IP',
                        'USER_TYPE_ID' => 'string',
                        'MANDATORY' => 'Y',
                        'EDIT_FORM_LABEL' => ['ru' => 'IP'],
                        'LIST_COLUMN_LABEL' => ['ru' => 'IP'],
                        'LIST_FILTER_LABEL' => ['ru' => 'IP'],
                        'ERROR_MESSAGE' => ['ru' => '', 'en' => ''],
                        'HELP_MESSAGE' => ['ru' => '', 'en' => ''],
                    ],
                    'UF_CITY' => [
                        'ENTITY_ID' => $ufObject,
                        'FIELD_NAME' => 'UF_CITY',
                        'USER_TYPE_ID' => 'string',
                        'MANDATORY' => 'Y',
                        'EDIT_FORM_LABEL' => ['ru' => Loc::getMessage('UF_CITY')],
                        'LIST_COLUMN_LABEL' => ['ru' => Loc::getMessage('UF_CITY')],
                        'LIST_FILTER_LABEL' => ['ru' => Loc::getMessage('UF_CITY')],
                        'ERROR_MESSAGE' => ['ru' => '', 'en' => ''],
                    ],
                ];
                foreach ($arFields as $arCartField) {
                    $obUserField = new CUserTypeEntity;
                    $obUserField->Add($arCartField);
                }
            }
        }
    }


    public function DoUninstall(): void
    {
        $this->UnInstallTasks();
        $this->UnInstallEvents();
        $this->UnInstallDB();
        $this->UnInstallFiles();

        ModuleManager::unRegisterModule($this->MODULE_ID);
    }

    /**
     * @return void
     * @throws ArgumentException
     * @throws LoaderException
     * @throws ObjectPropertyException
     * @throws SystemException
     */
    public function UnInstallDB(): void
    {
        Loader::requireModule('highloadblock');
        $arHLBlock = HighloadBlockTable::getList([
            'filter' => [
                '=TABLE_NAME' => 'deha_geoip_hl',
            ],
            'select' => ['ID'],
        ])->fetchAll();
        foreach ($arHLBlock as $item) {
            HighloadBlockTable::delete($item['ID']);
        }
    }

    public function UnInstallFiles(): void
    {
        $basePath = dirname(Loader::getLocal('modules/' . 'deha.geoip' . '/install/index.php'), 4);
        \Bitrix\Main\IO\Directory::deleteDirectory($basePath . '/components/' . $this->MODULE_ID);
        $documentRoot = Application::getDocumentRoot();
        foreach (new DirectoryIterator(__DIR__ . '/files/public/') as $item) {
            if ($item->isDir() && !$item->isDot()) {
                \Bitrix\Main\IO\Directory::deleteDirectory($documentRoot . '/' . $item->getFilename());
            }
        }
    }
}

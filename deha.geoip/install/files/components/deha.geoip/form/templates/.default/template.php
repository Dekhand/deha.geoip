<?php

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\UI\Extension;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
/** Bitrix
 *
 * @var array $arParams
 * @var array $arResult
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @var CBitrixComponentTemplate $this
 * @var CBitrixComponent $component
 */
Extension::load(['ui.forms', 'ui.buttons.icons']);
?>
<div>
    <div class='geoip_form'>
        <div class='ui-ctl ui-ctl-textbox ui-ctl-after-icon'>
            <button class='ui-ctl-after ui-ctl-icon-clear' id="<?= $arResult['CODE'] ?>_clear"></button>
            <input type='text' class='ui-ctl-element' id="<?= $arResult['CODE'] ?>_input"
                   value='217.15.204.156'>
        </div>
        <button class='ui-btn ui-btn-primary ui-btn-icon-search' id="<?= $arResult['CODE'] ?>_search">
            <?= Loc::getMessage('DGF_SEARCH') ?>
        </button>
    </div>
    <div class="geoip_form_result" id="<?= $arResult['CODE'] ?>_result">
    </div>
</div>
<script type='text/javascript'>
    <?php
    $lang = array_filter(Loc::loadLanguageFile(__FILE__), static function ($key) {
        return str_starts_with($key, 'JS_');
    }, ARRAY_FILTER_USE_KEY);
    ?>
    BX.message(<?=CUtil::PhpToJSObject($lang)?>);
    BX.ready(function () {
        BX.Deha.GeoIp.Form.create('<?=$arResult['CODE']?>');
    });
</script>

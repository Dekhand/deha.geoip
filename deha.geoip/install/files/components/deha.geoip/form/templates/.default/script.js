BX.namespace('BX.Deha.GeoIp');
if (typeof (BX.Deha?.GeoIp?.Form) === 'undefined') {
    BX.Deha.GeoIp.Form = function (code) {
        this._code = code;
        this.input = BX(this._code + '_input');
        this.result = BX(this._code + '_result');
        BX.bind(BX(this._code + '_clear'), 'click', BX.delegate(this._clearHandler, this));
        BX.bind(BX(this._code + '_search'), 'click', BX.delegate(this._searchHandler, this));
    };
    BX.Deha.GeoIp.Form.prototype = {
        _clearHandler: function () {
            this.input.value = '';
        },

        _searchHandler: function (e) {
            let self = this;
            BX.ajax.runComponentAction('deha.geoip:form', 'getData', {
                mode: 'class', data: {
                    ip: this.input.value
                },
            })
                .then(function (response) {
                    if (response.status === 'success') {
                        BX.append(BX.create('div', {
                            attrs: {
                                className: 'geoip_form_item',
                            },
                            text: BX.message('JS_DGF_IP') + ' ' + response.data.ip + ' ' + BX.message('JS_DGF_CITY')
                                + ' ' + response.data.city
                        }), self.result);
                    }
                })
                .catch(function (response) {
                    BX.append(BX.create('div', {
                        attrs: {
                            className: 'geoip_form_item geoip_form_item__error',
                        },
                        text: response.errors[0].message,
                    }), self.result);
                });
        }
    };
    BX.Deha.GeoIp.Form._self = {};
    BX.Deha.GeoIp.Form.create = function (code) {
        if (!this._self.hasOwnProperty(code)) {
            this._self[code] = new BX.Deha.GeoIp.Form(code);
        }
        return this._self[code];
    };
}

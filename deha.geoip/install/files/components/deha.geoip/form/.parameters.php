<?php

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);
$arComponentParameters = [
    'GROUPS' => [
        'SETTINGS' => [
            'NAME' => Loc::getMessage('FORM_GROUP_SETTINGS'),
        ],
    ],
    'PARAMETERS' => [
        'CACHE_TIME' => [],
    ],
];

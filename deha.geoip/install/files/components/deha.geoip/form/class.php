<?php

use Bitrix\Main\Engine\Contract\Controllerable;
use Bitrix\Main\Engine\Response\AjaxJson;
use Bitrix\Main\ErrorCollection;
use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;
use Bitrix\Main\Localization\Loc;
use Deha\GeoIp\GeoIpClient;
use Psr\Http\Client\ClientExceptionInterface;

class FormComponent extends CBitrixComponent implements Controllerable
{

    public function executeComponent(): void
    {
        try {
            Loader::requireModule('deha.geoip');
            $this->arResult['CODE'] = $this->randString();

            $this->includeComponentTemplate();
        } catch (LoaderException) {
            ShowError(Loc::getMessage('DG_MODULE_NOT_INSTALLED'));
        } catch (Exception $exception) {
            ShowError($exception->getMessage());
        }
    }

    public function configureActions(): array
    {
        return [];
    }

    /**
     * @param string $ip
     * @return AjaxJson
     * @throws ClientExceptionInterface
     */
    public function getDataAction(string $ip): AjaxJson
    {
        try {
            Loader::requireModule('deha.geoip');
            $client = new GeoIpClient();
            $data = $client->getInfo($ip);
            $result = $data->toArray();
            $result['ip'] = $ip;
        } catch (Exception $exception) {
            return AjaxJson::createError(
                new ErrorCollection(
                    [
                        new \Bitrix\Main\Error($exception->getMessage()),
                    ]
                )
            );
        }
        return AjaxJson::createSuccess($result);
    }
}

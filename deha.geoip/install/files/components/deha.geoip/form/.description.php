<?php

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$arComponentDescription = [
    'NAME' => Loc::getMessage('FORM_COMPONENT_NAME'),
    'DESCRIPTION' => Loc::getMessage('FORM_COMPONENT_DESCRIPTION'),
    'PATH' => [
        'ID' => 'content',
        'CHILD' => [
            'ID' => 'deha',
            'NAME' => Loc::getMessage('FORM_COMPONENT_VENDOR'),
        ],
    ],
    'CACHE_PATH' => 'Y',
    'COMPLEX' => 'N',
];

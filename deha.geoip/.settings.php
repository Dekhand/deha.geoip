<?php

use Bitrix\Main\Web\HttpClient;
use Deha\GeoIp\Services\SypexGeoNet;

return [
    'services' => [
        'value' => [
            'deha.geoip.client' => [
                'className' => HttpClient::class, //psr-18
            ],
            'deha.geoip.service' => [
                'className' => SypexGeoNet::class,
            ],
        ],
    ],
];
